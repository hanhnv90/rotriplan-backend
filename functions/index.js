'use strict';

const functions = require('firebase-functions');
const admin = require('firebase-admin');
const express = require('express');
const validator = require('validator');
const cors = require('cors');
const os = require('os');
const fs = require('fs');
const path = require('path');
const Busboy = require('busboy');
const nodemailer = require('nodemailer');

admin.initializeApp(functions.config().firebase);

const app = express();

// GET /api/messages?category={category}
// Get all messages, optionally specifying a category to filter on
app.get('/messages', (req, res) => {
    return res.status(200).json({"name" : "dung333"});
})

// POST /api/register
// Register new user on server
app.post('/signup', (req, res) => {
    let email = req.body.email;
    let password = req.body.password;
    // let gcmKey = req.body.gcmKey;
    let name = req.body.name;
    let phone = req.body.phone;
    let phonecodeid = req.body.phonecodeid;
    let logintype = req.body.logintype;
    // let image = req.query.image;
    let newUser = {
        "email" : email,
        "password" : password,
        "gcmKey" : "abc",
        "name" : name,
        "phone" : phone,
        "phonecodeid" : phonecodeid,
        "logintype" : logintype,
        "follower": [],
        "following": [],
        "block": [],
        "car": {
            productionYear: '',
            producer: '',
            model: '',
            gasTank: '',
            fuelType: '',
            mpg: '',
            fuelPrice: '' ,
            cityDrivingRate: '' 
        },
        "premium": false,
        "status": 'active',
        "thumbnail": 'https://firebasestorage.googleapis.com/v0/b/rotripplan.appspot.com/o/default-icon.svg?alt=media&token=92ba55db-7cb0-49b5-bbea-7e638188c0e7'
    }
    admin.firestore().collection('users').where('email', '==', email).get().then(snapshot => {
        if(snapshot.size > 0){
            let resultSignup = { 
                "message" : 'Account exists with same email',
                "status"  : false
            }
            return res.status(200).json(resultSignup);
         }
        admin.firestore().collection('users').add(newUser).then((writeResult) => {
            var userResult = {};
            userResult.id = writeResult.id
            userResult.email =  req.body.email;
            userResult.message = "success";
            userResult.status = true;
            
            admin.firestore().collection('users').doc(userResult.id).update({
                "id" : userResult.id,
            })
            return res.json(userResult);
        }).catch((error) => {
            let resultSignup = { 
                "message" : 'Error',
                "status"  : false
            }
            return res.status(200).json(resultSignup);
        });
       return "TODO";
    }).catch((error) => {
        let result = {
            "message": "An error occurred",
            "status" : false
        }
        return res.status(200).json(result);
    });
    
})
// POST /api/logout
// logout on server
app.post('/logout', (req, res) => {
    let userId = req.body.userId;
    admin.auth().signOut(userId).then(() => {
        // Sign-out successful.
        return "TODO";
    }).catch((error) => {
    // An error happened.
    });
})
// GET /api/viewprofile
// viewprofile on server
app.get('/viewprofile', (req, res) => {
    let userid = req.query.userid;
    admin.firestore().collection('users').doc(userid).get().then((profileResult) => {
        // Send back a message that we've succesfully written the message
        var profile  = 
            {
                viewprofile : {}
            }
        let user = profileResult.data();
        delete user.password;
        delete user.email;

        // profile.viewprofile.id = profileResult.id;
        // profile.viewprofile.name = profileResult._fieldsProto.name.stringValue;
        // // profile.viewprofile.image = profileResult._fieldsProto.image.stringValue;
        // if(profileResult._fieldsProto.phonecodeid === undefined){
        //     profile.viewprofile.phonecodeid = ""
        // }else{
        //     profile.viewprofile.phonecodeid = profileResult._fieldsProto.phonecodeid.stringValue;
        // }
        // if(profileResult._fieldsProto.phone === undefined){
        //     profile.viewprofile.phonecodeid = ""
        // }else{
        //     profile.viewprofile.phone = profileResult._fieldsProto.phone.stringValue;
        // }
        // if(profileResult._fieldsProto.lat !== undefined){
        //     profile.viewprofile.lat = profileResult._fieldsProto.lat.stringValue;
        // }
        // if(profileResult._fieldsProto.lng !== undefined){
        //     profile.viewprofile.lng = profileResult._fieldsProto.lng.stringValue;
        // }
        
        profile.profile = user;
        profile.success = "success";        
        profile.message = true;
        profile.status = true;
        return res.json(profile);
    }).catch((error) => {
            let result = {
                "message": "An error occurred",
                "status" : false
            }
            return res.status(200).json(result);
    });
})

//API update profile in post
app.post('/updateprofile', (req, res) => {
    let userid = req.body.userid;
    let gcmKey = req.body.abc;
    let name = req.body.name;
    let phone = req.body.phone;
    let phonecodeid = req.body.phonecodeid;
    let logintype = req.body.logintype;
    let thumbnail = req.body.thumbnail;
    let productionYear = req.body.productionYear;
    let producer = req.body.producer;
    let model = req.body.model;
    let gasTank = req.body.gasTank;
    let fuelType = req.body.fuelType;
    let mpg = req.body.mpg;
    let fuelPrice = req.body.fuelPrice;
    let cityDrivingRate = req.body.cityDrivingRate;
    let premium = req.body.premium;

    // Create a reference to the SF doc.
    var sfDocRef = admin.firestore().collection("users").doc(userid);

    admin.firestore().runTransaction(transaction => {
        return transaction.get(sfDocRef).then(snapshot => {
            let dataUpdate = {};
            if (gcmKey !== undefined) 
                dataUpdate.gcmKey = gcmKey;
            if (name !== undefined) 
                dataUpdate.name = name;
            if (phone !== undefined)
                dataUpdate.phone = phone;
            if (phonecodeid !== undefined)
                dataUpdate.phonecodeid = phonecodeid;
            if (logintype !== undefined) 
                dataUpdate.logintype = logintype;
            if (thumbnail !== undefined)
                dataUpdate.thumbnail = thumbnail;

            // car's information
            dataUpdate.car = snapshot.get('car');
            if (productionYear !== undefined) 
                dataUpdate.car.productionYear = productionYear;
            if (producer !== undefined)
                dataUpdate.car.producer = producer;
            if (model !== undefined)
                dataUpdate.car.model = model;
            if (gasTank !== undefined)
                dataUpdate.car.gasTank = gasTank;
            if (fuelPrice !== undefined)
                dataUpdate.car.fuelPrice = fuelPrice;
            if (fuelType !== undefined)
                dataUpdate.car.fuelType = fuelType;
            if (cityDrivingRate !== undefined)
                dataUpdate.car.cityDrivingRate = cityDrivingRate;
            if (mpg !== undefined)
                dataUpdate.car.mpg = mpg;

            // premium update
            if (premium !== undefined) 
                dataUpdate.premium = premium;

            transaction.update(sfDocRef, dataUpdate);
            return "TODO";
        });
    }).then(() => {
        console.log("Transaction successfully committed!");
        var result = {};
        result.message = "success";
        result.status = true;
        return res.json({result:result});
    }).catch((error) => {
        console.log("Transaction failed: ", error);
        res.status(200).send("Failed!");
    });
})

// GET /api/notificationlog
// NOTIFICATION on server
app.get('/notificationlog', (req, res) => {
    let userId = req.query.userId;
    let page  = req.query.page ;

    admin.firestore().collection('notificationLog').where('userId', '==', userId).get()
    .then(snapshot => {
        let countNotificationlog = snapshot.size;
        let notificationLogResult = {
            notificationLog : []
        };
        let newNotification = {};
     
        for (var i = 0; i < snapshot.size; i++) { 
            if(snapshot.docs[i]._fieldsProto.id !== undefined){
                newNotification.id = snapshot.docs[i]._fieldsProto.id.stringValue;
            }
            if(snapshot.docs[i]._fieldsProto.message !== undefined){
                newNotification.message = snapshot.docs[i]._fieldsProto.message.stringValue;
            }
            if(snapshot.docs[i]._fieldsProto.notificationStatus !== undefined){
                newNotification.notificationStatus = snapshot.docs[i]._fieldsProto.notificationStatus.stringValue;
            }
            if(snapshot.docs[i]._fieldsProto.tripId !== undefined){
                newNotification.tripId = snapshot.docs[i]._fieldsProto.tripId.stringValue;
            }
            if(snapshot.docs[i]._fieldsProto.time !== undefined){
                newNotification.time = snapshot.docs[i]._fieldsProto.time.stringValue;
            }
            if(snapshot.docs[i]._fieldsProto.date !== undefined){
                newNotification.date = snapshot.docs[i]._fieldsProto.date.timestampValue;
            }
            if(snapshot.docs[i]._fieldsProto.tripName !== undefined){
                newNotification.tripName = snapshot.docs[i]._fieldsProto.tripName.stringValue;
            }
            if(snapshot.docs[i]._fieldsProto.teamName !== undefined){
                newNotification.teamName = snapshot.docs[i]._fieldsProto.teamName.stringValue;
            }
            if(snapshot.docs[i]._fieldsProto.userId !== undefined){
                newNotification.userId = snapshot.docs[i]._fieldsProto.userId.stringValue;
            }
            notificationLogResult.notificationLog.push(newNotification);
        }
        notificationLogResult.totalItems = countNotificationlog;
        notificationLogResult.totalPage = 10;
        notificationLogResult.page = Math.floor(notificationLogResult.totalItems/notificationLogResult.totalPage);
        
        return res.json({result: notificationLogResult});
    }).catch((error) => {
        console.error("Error removing document: ", error);
    });
    
})

// POST /api/accepttrip
// ACCEPT_TRIP on server
app.post('/accepttrip', (req, res) => {
    var userId = req.body.userId;
    var tripId = req.body.tripId;
    var status = req.body.status;

    // Create a reference to the SF doc.
    var sfDocRef = admin.firestore().collection("trips").doc(tripId);

    admin.firestore().runTransaction(transaction => {
        return transaction.get(sfDocRef).then(snapshot => {
            const memWallet = snapshot.get('memWallet');
            const newMember = snapshot.get('members');
            newMember.push(userId);
            memWallet[userId] = {
                'status': true,
                'cost': 0,
                'paid': 0,
                'acceptTime': new Date().getTime()
            };
            transaction.update(sfDocRef, {"members": newMember, "memWallet": memWallet});
            return "TODO";
        });
    }).then(() => {
        console.log("Transaction successfully committed!");
        res.status(200).send("Successful!");
        var result = {};
        result.message = "success";
        result.status = true;
        return res.json({result:result});
    }).catch((error) => {
        console.log("Transaction failed: ", error);
        res.status(200).send("Failed!");
    });
});     

// POST /api/accepttrip
// INVITE_FRIENDS on server
app.post('/invitation', (req, res) => {
    let userid = req.body.userid;
    let tripid = req.body.tripid;
    let members  = req.body.members;
    let newInvitation = {
        "userid" : userid,
        "tripid" : tripid,
        "members " : members,
    }
    var sfDocRef = admin.firestore().collection("trips").doc(tripId);

    admin.firestore().runTransaction(transaction => {
            return transaction.get(sfDocRef, members).then(snapshot => {
                for(var i=0 ;i< members.size(); i++){
                    const newMember = snapshot.get('i');
                    newMember[i] = true;
                    transaction.update(sfDocRef, 'i', newMember);
                }
                return "TODO"; 
            });
    }).then(() => {
        var inviteFriend = {};
        inviteFriend.message = "success";
        inviteFriend.status = true;
        console.log("Transaction successfully committed!");
        res.status(200).send("Successful!");
        return res.json({result: inviteFriend});
    }).catch((error) => {
        console.log("Transaction failed: ", error);
        res.status(200).send("Failed!");
    });
});
// POST /api/requestjointrip
// REQUEST_JOIN_TRIP on server
app.post('/requestjointrip', (req, res) => {
    let userid = req.body.userid;
    let tripid = req.body.tripid;
    let newRequestJoinTrip = {
        "userid" : userid,
        "tripid" : tripid
    }
    admin.firestore().collection('trip').add(newRequestJoinTrip).then((requestJoinTripResult) => {
        var requestJoinTrip = {};
        requestJoinTrip.message = "success";
        requestJoinTrip.status = true;
        // Send back a message that we've succesfully written the message
        return res.json({result: requestJoinTrip});
    }).catch((error) => {
        console.error("Error removing document: ", error);
    });
});
// POST /api/acceptjointrip
// ACCEPT_JOIN_TRIP on server
app.post('/acceptjointrip', (req, res) => {
    let userId = req.body.userId;
    let tripId = req.body.tripId;
    let status = req.body.status;
    // let newAcceptjointrip = {
    //     "userId" : userId,
    //     "tripId" : tripId,
    //     "status" : status
    // }
    // Create a reference to the SF doc.
    var sfDocRef = admin.firestore().collection("trips").doc(tripId);

    admin.firestore().runTransaction(transaction => {
        return transaction.get(sfDocRef, userId).then(snapshot => {
            const memWallet = snapshot.get('memWallet');
            const newMember = snapshot.get('members');
            newMember.push(userId);
            memWallet[userId] = {
                'status': true,
                'cost': 0,
                'paid': 0,
                'acceptTime': new Date().getTime()
            };
            transaction.update(sfDocRef, {"members": newMember, "memWallet": memWallet});
            return "TODO";
        });
    }).then(() => {
        var result = {};
        result.userId = req.body.userId;
        result.tripId = req.body.tripId;
        result.status =req.body.status;
        return res.json({result: result});
    }).catch((error) => {
        console.log("Transaction failed: ", error);
        res.status(200).send("Failed!");
    });
});
// POST /api/notificationlogdelete
// DELETE_NOTIFICATION on server
app.post('/notificationlogdelete', (req, res) => {
    let userid = req.body.userid;
    let notificationids = req.body.notificationids;
    return  admin.firestore().collection("notificationLog").doc(notificationids).delete().then(() => {
        var delNotiResult = {};
        delNotiResult.message = "success";
        delNotiResult.status = true;
        res.status(200).send("Successful!");
        return res.json({result: delNotiResult});
    }).catch((error) => {
        res.status(200).send("Failed!");
    });
});
// POST /api/forgotpassword
// FORGOT_PASSWORD on server
app.post('/forgotpassword', (req, res) => {
    let email = req.body.email;
    // const gmailEmail = functions.config().gmail.email;
    // const gmailPassword = functions.config().gmail.password;

    const gmailEmail = 'hanhnv90@gmail.com';
    const gmailPassword = 'Trybad!@#1';

    const mailTransport = nodemailer.createTransport({
      service: 'gmail',
      auth: {
        user: gmailEmail,
        pass: gmailPassword,
      },
    });
    var randomPassword = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    for (var i = 0; i < 5; i++){
        randomPassword += possible.charAt(Math.floor(Math.random() * possible.length));
    }
    const mailOptions = {
        from: '"Spammy Corp." <noreply@firebase.com>',
        to: 'hanhnv90@gmail.com',
        html: '<p>New password is: ' + randomPassword + '</p>',
        subject: 'reset passowd'
    };
    mailOptions.password =  randomPassword;
    return mailTransport.sendMail(mailOptions).then(() => {
        return admin.firestore().collection("users").where('email', '==', email).get().then(users => {
            if (users.size === 1) {
                users.forEach(userInfo => {
                    let user = userInfo.data();
                    admin.firestore().collection('users').doc(user.id).update({
                        "status" : 'inactive',
                        "password": randomPassword
                    });
                    var result = {};
                    result.message = "success";
                    result.status = true;
                    result.email = email;
                    result.password = randomPassword;
                    result.id = user.id;
                    return res.json({result: result});
                })
            } else {
                return res.status(200).send("Error");
            }
            return "TODO";
        });
    }).catch((error) => {
        console.log(error);
        return res.status(200).send("Error");
    });
});

app.post('/activeUser', (req, res) => {
    let email = req.body.email;
    let password = req.body.password;

    return admin.firestore().collection("users").where('email', '==', email).get().then(users => {
        if (users.size === 1) {
            users.forEach(userInfo => {
                let user = userInfo.data();
                if (user.password !== undefined 
                        && password === user.password
                        && user.status !== undefined
                        && user.status === 'inactive') {
                    admin.firestore().collection('users').doc(user.id).update({
                        "status" : 'active'
                    });
                    var result = {};
                    result.message = "success";
                    result.status = true;
                    result.email = email;
                    result.id = user.id;
                    return res.json({result: result});
                } else {
                    return res.status(200).send("Error");        
                }
            })
        } else {
            return res.status(200).send("Error");
        }
        return "TODO";
    }).catch((error) => {
        console.log(error);
        return res.status(200).send("Error");
    });
});

// POST /api/login
// Login with user
app.post('/login', (req, res) => {
    let email = req.body.email;
    if (!email) {
        res.status(200).send("Please enter email.");
        return;
    }
    if(!validator.isEmail(email)){
        res.status(200).send("Please enter valid email.");
        return;
    }
        
    let password = req.body.password;
    if (!password) {
        res.status(200).send("Please enter password.");
        return;
    }

    // let gcmKey = req.body.gcmKey;
    // if (!gcmKey) {
    //     res.status(200).send("Missing gcmKey.");
    //     return;
    // }

    var userRef = admin.firestore().collection('users')

    userRef.where('email', '==', email).get().then(snapshot => {
        if(snapshot.size < 1){
            let currentUser = { 
                "message" : 'Email does not exist',
                "status"  : false
            }
            return res.status(200).json(currentUser);
        }
        snapshot.forEach(doc => {
            console.log(doc.id, '=>', doc.data().name);
            var userPass = doc.data().password;

            if(userPass && password === userPass){
                let currentUser = { 
                    "id"      : doc.id, 
                    "email"   : doc.data().email, 
                    "message" : 'Success',
                    "status"  : true
                }
                return res.status(200).json(currentUser);
            }else{
                let currentUser = { 
                    "message" : 'Incorrect username or password',
                    "status"  : false
                }
                return res.status(200).json(currentUser);
            }
        });
        let currentUser = { 
            "message" : 'Email does not exist',
            "status"  : false
        }
        return res.status(200).json(currentUser);
    })
    .catch(err => {
        console.log('Error', err);
        throw res.status(200).send("Error");
    });
})

// POST /api/sociallogin
// Login with facebook
app.post('/sociallogin', (req, res) => {
    let name = req.body.name;
    if (!name) {
        res.status(200).send("Please enter name.");
        return;
    }

    let accessToken = req.body.accesstoken;
    if (!accessToken) {
        res.status(200).send("Missing accessToken.");
        return;
    }
    
    let email = req.body.email;
    if (!email) {
        res.status(200).send("Please enter email.");
        return;
    }
    if(!validator.isEmail(email)){
        res.status(200).send("Please enter valid email.");
        return;
    }
        
    let loginType = req.body.logintype;
    if (!loginType) {
        res.status(200).send("Please enter loginType.");
        return;
    }

    let gcmKey = req.body.gcmkey;
    if (!gcmKey) {
        res.status(200).send("Missing gcmKey.");
        return;
    }

    var userRef = admin.firestore().collection('users')

    let newUser = { 
        "name"        : name,
        "accessToken" : accessToken, 
        "email"       : email, 
        "loginType"   : loginType, 
        "gcmKey"      : gcmKey,
        "follower"    : [],
        "following"   : [],
        "block": [],
        "car": {
            productionYear: '',
            producer: '',
            model: '',
            gasTank: '',
            fuelType: '',
            mpg: '',
            fuelPrice: '' ,
            cityDrivingRate: '' 
        },
        "premium": false,
        "status": 'active',
        "thumbnail": 'https://firebasestorage.googleapis.com/v0/b/rotripplan.appspot.com/o/default-icon.svg?alt=media&token=92ba55db-7cb0-49b5-bbea-7e638188c0e7'
    }
    userRef.where('email', '==', email).get().then(snapshot => {
        let currentUser = {}
        if(snapshot.size === 0){
            admin.firestore().collection('users').add(newUser).then((writeResult) => {
                currentUser = { 
                    "id"      : writeResult.id, 
                    "email"   : writeResult.data().email, 
                    "message" : 'Success',
                    "status"  : true
                }
                return res.status(200).json(currentUser);
            })
            .catch(err => {
                console.log('Error', err);
                throw res.status(200).send("Error");
            });
        }else{
            snapshot.forEach(doc => {
                currentUser = { 
                    "id"      : doc.id, 
                    "email"   : doc.data().email, 
                    "message" : 'Success',
                    "status"  : true
                }
                return res.status(200).json(currentUser);
            });
        }
        let result = { 
            "message" : 'User Not Found',
            "status"  : false
        }
        return res.status(200).json(result);
    })
    .catch((error) => {
        let result = {
            "message": "An error occurred",
            "status" : false
        }
        return res.status(200).json(result);
    });
})

// GET /api/phonecode
// Get all phonecode
app.get('/phonecode', (req, res) => {
    let phonecodes={};
    let phonecodelist=[];
    let output = {
        "phoneCodeList" : phonecodelist,
        "message"       : "Success",
        "status"        : true
    }
    admin.firestore().collection('phonecode').get().then((snapshot) => {
        snapshot.forEach(doc => {
            phonecodes = doc.data();
            phonecodelist.push(phonecodes);
        });
        return res.status(200).json(output);
    }).catch(err => {
        console.log('Error', err);
        throw res.status(200).send("Error");
    });
})

// POST /api/createtrip
// Create trip
app.post('/createtrip', (req, res) => {
    let ccid        = req.body.ccid;
    let tripname    = req.body.tripname;
    let description = req.body.description;
    if (!description) {
        description = "N/A";
    }
    let startdate   = req.body.startdate;
    let enddate     = req.body.enddate;
    let starttime   = req.body.starttime;
    let endtime     = req.body.endtime;
    let startpoint  = req.body.startpoint;
    let endpoint    = req.body.endpoint;
    let startlat    = req.body.startlat;
    let startlng    = req.body.startlng;
    let endlat      = req.body.endlat;
    let endlng      = req.body.endlng;
    let image       = req.body.image;
    let triptype    = req.body.triptype;
    let roundtrip   = req.body.roundtrip;
    let status      = req.body.status;

    let waypointsStr = req.body.waypoints;
    let members     = req.body.members;
    let isComment   = req.body.isComment;
    let showTeamMems   = req.body.showTeamMems;
    let maxMembers = req.body.showTeamMems;

    let memWallet = {};
    // let members = [];
    let waypoint = {};
    let expenses = {};

    let usersComment = [];
    let comments = [];
    let likes = [];

    let newTrip = {
        "ccid"        : ccid,
        "tripname"    : tripname,
        "description" : description,
        "startdate"   : startdate,
        "enddate"     : enddate,
        "starttime"   : starttime,
        "endtime"     : endtime,
        "startpoint"  : startpoint,
        "endpoint"    : endpoint,
        "startlat"    : startlat,
        "startlng"    : startlng,
        "endlat"      : endlat,
        "endlng"      : endlng,
        "image"       : image,
        "triptype"    : triptype,
        "roundtrip"   : roundtrip,
        "members"     : members,
        "memWallet"   : memWallet,
        "waypoints"   : waypoint,
        "total_cost"  : 0,
        "total_paid"  : 0,
        "expenses"    : expenses,
        "comments"    : comments,
        "usersComment": usersComment,
        "likes"       : likes,
        "viewsCount"  : 0,
        "likesCount"  : 0,
        "commentsCount": 0,
        "status"      : status,
        "createdTime" : new Date().getTime(),
        "follow"      : [],
        "isComment"   : isComment,
        "showTeamMems": showTeamMems,
        "maxMembers"  : maxMembers
    }

    var idWaypointList=[]; 
    var idMemberList=[]; 
    var waypoints = JSON.parse(waypointsStr);
    for (var i = 0; i < waypoints.length; i++) {
        var objWaypoint = waypoints[i];
        admin.firestore().collection('waypoints').add(objWaypoint).then((snapshot) => {
            var snapshotId = snapshot.id;
            admin.firestore().collection('waypoints').doc(snapshotId).update({
                "id" : snapshotId,
            })
            idWaypointList.push(snapshotId);
            return "TODO";
        }).catch(err => {
            console.log('Error', err);
            throw res.status(200).send("Error");
        });
    }

    // for (var j = 0; j < members.length; j++) {
    //     var objMember = members[j];
    //     admin.firestore().collection('members').add(objMember).then((snapshot) => {
    //         var snapshotId = snapshot.id;
    //         admin.firestore().collection('members').doc(snapshotId).update({
    //             "id" : snapshotId,
    //         })
    //         idMemberList.push(snapshotId);
    //         return "TODO";
    //     }).catch(err => {
    //         console.log('Error', err);
    //         throw res.status(200).send("Error");
    //     });
    // }

    return admin.firestore().collection('trips').add(newTrip).then((snapshot) => {
        var output = {
            "message"       : "Success",
            "tripid"        : snapshot.id,
            "status"        : true
        }
        var sfDocRef = admin.firestore().collection("trips").doc(snapshot.id);
        admin.firestore().runTransaction(transaction => {
            return transaction.get(sfDocRef).then(snapshots => {
                const newMember = snapshots.get('members');
                const newWaypoint = snapshots.get('waypoints');
                const newMemWallet = snapshots.get('memWallet');

                // update tripId
                transaction.update(sfDocRef, 'tripid', snapshot.id);

                for (var i=0; i < idMemberList.length;i++){
                    newMemWallet[idMemberList[i]] = {
                        'status': true,
                        'cost': 0,
                        'paid': 0,
                        'acceptTime': new Date().getTime()
                    };
                    if (newMember.indexOf(idMemberList[i]) === -1) 
                        newMember.push(idMemberList[i]);
                }
                transaction.update(sfDocRef, {"memWallet": newMemWallet, "members": members});

                for (var j=0; j < idWaypointList.length;j++){
                    newWaypoint[idWaypointList[j]] = true;
                    transaction.update(sfDocRef, 'waypoints', newWaypoint);
                }
                return "TODO";
            });
        }).then(() => {
            return res.status(200).send("Transaction successfully committed!")
        }).catch((error) => {
            console.log("Transaction failed: ", error);
        });
        return res.status(200).json(output);
    }).catch((error) => {
        var output = {
            "message"       : "An error occurred while creating trip !",
            "status"        : false
        }
        return res.status(200).json(output);
    });
})


//GET api/mytrip
//GET_MY_TRIPS
app.get('/mytrip', (req, res) => {
    let page   = req.query.page;
    let userid = req.query.userid;
    var getMyTrips ={};
    var totalItems, totalPage;
    
    return admin.firestore().collection('trips').where("ccid", "==", userid).get().then((myTripResult) => {
        let queryResults = [];
        let myTripList = [];
        let queryMemberResults = [];
        totalItems = myTripResult.size;
        if(page === undefined || page === 0){
            totalPage = 0;
        }else{
            totalPage = Math.floor(totalItems / page);
        }
        
        myTripResult.forEach((doc) => {
            var waypointIdsRef = [];
            for (var id in doc.data().waypoints){
                waypointIdsRef.push(admin.firestore().doc('waypoints/' + id));
            }
            for (var idMember in doc.data().member){
                admin.firestore().collection('users').doc(idMember).get().then((usersResult) => {
                    queryMemberResults.push(usersResult.data());
                    return "TODO";
                }).catch((error) => {
                    let result = {
                        "message": "An error occurred",
                        "status" : false
                    }
                    return res.status(200).json(result);
                });
            }
            myTripList.push(doc.data());
            queryResults.push(admin.firestore().getAll(waypointIdsRef));
        })

        return Promise.all(queryResults).then(tripsWaypoints => {
            var i = 0;
            tripsWaypoints.forEach((waypoints) => {
                var waypointArr = [];
                waypoints.forEach(function(waypoint) {
                    waypointArr.push(waypoint.data());
                });
                
                var trip = myTripList[i++];
                trip.waypoints = waypointArr;
                trip.member = queryMemberResults;
            });

            getMyTrips = {
                "mytrip" : myTripList,
                "totalpage": totalPage,
                "page": page,
                "totalitems": totalItems,
                "status": true,
                "message": "Success"
            };

            return res.status(200).json(getMyTrips);
        })
    }).catch((error) => {
        let result = {
            "message": "An error occurred",
            "status" : false
        }
        return res.status(200).json(result);
    });
})

//GET api/tripijoin
//GET_TRIPS_I_JOINED
app.get('/tripijoin', (req, res) => {
    let page   = req.query.page;
    let userid = req.query.userid;
    let memberid = "member."+ userid;
    var getTripIjoined ={};
    let queryMemberResults = [];
    var totalItems, totalPage;

    return admin.firestore().collection('trips').where(memberid, '==', true).get().then((tripIjoinedResult) => {
        let queryResults = [];
        let tripIJoinList = [];
        totalItems = tripIjoinedResult.size;
        if(page === undefined || page === 0){
            totalPage = 0;
        }else{
            totalPage = Math.floor(totalItems / page);
        }

        tripIjoinedResult.forEach((doc) => {
            var waypointIdsRef = [];
            for (var id in doc.data().waypoints){
                waypointIdsRef.push(admin.firestore().doc('waypoints/' + id));
            }
            for (var idMember in doc.data().member){
                admin.firestore().collection('users').doc(idMember).get().then((usersResult) => {
                    queryMemberResults.push(usersResult.data());
                    return "TODO";
                }).catch((error) => {
                    let result = {
                        "message": "An error occurred",
                        "status" : false
                    }
                    return res.status(200).json(result);
                });
            }
            tripIJoinList.push(doc.data());
            queryResults.push(admin.firestore().getAll(waypointIdsRef));
        })

        return Promise.all(queryResults).then(tripsWaypoints => {
            var i = 0;
            tripsWaypoints.forEach((waypoints) => {
                var waypointArr = [];
                waypoints.forEach(function(waypoint) {
                    waypointArr.push(waypoint.data());
                });
                
                var trip = tripIJoinList[i++];
                trip.waypoints = waypointArr;
                trip.member = queryMemberResults;
            });

            getTripIjoined = {
                "tripijoin" : tripIJoinList,
                "totalpage": Math.floor(totalItems / page),
                "page": page,
                "totalItems": totalItems,
                "status": true,
                "message": "Success"
            }
            return res.status(200).json(getTripIjoined);
        })
    }).catch((error) => {
        let result = {
            "message": "An error occurred",
            "status" : false
        }
        return res.status(200).json(result);
    });
})

//GET api/tripnearby
//GET_TRIPS_NEARBY
app.get('/tripnearby', (req, res) => {
    let page   = req.query.page;
    var userid = req.query.userid;
    let lat    = req.query.lat;
    let lng    = req.query.lng;
    let memberid = "member."+ userid;
    var getTripNearby ={};
    let queryMemberResults = [];
    var latMember, lngMember, totalPage, totalItems;

    return admin.firestore().collection('trips').get().then((tripResult) => {
        let queryResults = [];
        let tripNearbyList = [];
        totalItems = tripResult.size;
        if(page === undefined || page === 0){
            totalPage = 0;
        }else{
            totalPage = Math.floor(totalItems / page);
        }
      
        if (tripResult.size > 0){
            for (var i=0; i< tripResult.size; i++){
                const doc = tripResult.docs[i];
                latMember = doc.data().startlat;
                lngMember = doc.data().startlng;
                
                var a = Math.pow(latMember-lat,2);
                var b = Math.pow(lngMember-lng,2);
    
                var result = Math.sqrt(a+b);
                
                if (result <= 50) {
                    var waypointIdsRef = [];
                    for (var id in doc.data().waypoints){
                        waypointIdsRef.push(admin.firestore().doc('waypoints/' + id));
                    }
                    for (var idMember in doc.data().member){
                        admin.firestore().collection('users').doc(idMember).get().then((usersResult) => {
                            queryMemberResults.push(usersResult.data());
                            return "TODO";
                        }).catch((error) => {
                            let result = {
                                "message": "An error occurred",
                                "status" : false
                            }
                            return res.status(200).json(result);
                        });
                    }
                    tripNearbyList.push(doc.data());

                    queryResults.push(admin.firestore().getAll(waypointIdsRef));
                }
            }
        }

        return Promise.all(queryResults).then(tripsWaypoints => {
            var i = 0;
            tripsWaypoints.forEach((waypoints) => {
                var waypointArr = [];
                waypoints.forEach(function(waypoint) {
                    waypointArr.push(waypoint.data());
                });
                
                var trip = tripNearbyList[i++];
                trip.waypoints = waypointArr;
                trip.member = queryMemberResults;
            });

            getTripNearby = {
                "tripnearby" : tripNearbyList,
                "totalpage": Math.floor(totalItems / page),
                "page": page,
                "totalItems": totalItems,
                "status": true,
                "message": "Success"
            }
            return res.status(200).send(getTripNearby);
        })
    }).catch((error) => {
        let result = {
            "message": "An error occurred",
            "status" : false
        }
        return res.status(200).json(result);
    });
})

//GET api/tripdetail
//GET_TRIPS_DETAIL
app.get('/tripdetail', (req, res) => {
    let tripid = req.query.tripid;
    var getTripDetail ={};
    let queryMemberResults = [];
    return admin.firestore().collection('trips').doc(tripid).get().then((tripDetail) => {
        let queryResults = [];
        let resultTripDetail = [];
        var waypointIdsRef = [];
        var ccdetailArr = [];
        if(tripDetail.size === 0){
            let result = {
                "message": "Trip does not exist",
                "status" : false
            }
            return res.status(200).json(result);
        }
       
        for (var id in tripDetail.data().waypoints){
            waypointIdsRef.push(admin.firestore().doc('waypoints/' + id));
        }
        for (var idMember in tripDetail.data().member){
            let member = {};
            admin.firestore().collection('users').doc(idMember).get().then((usersResult) => {
                member = usersResult.data();
                delete member.email;
                delete member.password;
                queryMemberResults.push(member);
                return "TODO";
            }).catch((error) => {
                let result = {
                    "message": "An error occurred",
                    "status" : false
                }
                return res.status(200).json(result);
            });
        }
        resultTripDetail = tripDetail.data();
        queryResults.push(admin.firestore().getAll(waypointIdsRef));

        return Promise.all(queryResults).then(tripsWaypoints => {
            var i = 0;
            tripsWaypoints.forEach((waypoints) => {
                var waypointArr = [];
                waypoints.forEach(function(waypoint) {
                    waypointArr.push(waypoint.data());
                });
                
                var trip = resultTripDetail;
                trip.waypoints = waypointArr;
                trip.member = queryMemberResults;
                if(trip.ccid === undefined){
                    trip.status = false;
                }else{
                     trip.status = true;
                }
            });
            
            return admin.firestore().collection('users').doc(resultTripDetail.ccid).get().then((tripIjoinedResult) => {
                let member = tripIjoinedResult.data();
                delete member.email;
                delete member.password;
                ccdetailArr.push(member);
                resultTripDetail.ccdetail = ccdetailArr;
                resultTripDetail.ccdetail[0].id = resultTripDetail.ccid;
                return res.status(200).json(resultTripDetail);
            })
        })
    }).catch((error) => {
        let result = {
            "message": "An error occurred",
            "status" : false
        }
        return res.status(200).json(result);
    });
})

//GET api/edittrip
//EDIT_TRIP
app.post('/edittrip', (req, res) => {
    let edittype    = req.body.edittype;
    if (!edittype) {
        res.status(200).send("Please enter edit type.");
        return;
    }
    let tripid      = req.body.tripid;
    let waypoint    = req.body.waypoint;
    let favourite   = req.body.favourite;
    let image       = req.body.image;
    let ccid        = req.body.ccid;
    let description = req.body.description;
    let startdate   = req.body.startdate;
    let enddate     = req.body.enddate;
    let starttime   = req.body.starttime;
    let endtime     = req.body.endtime;
    let triptype    = req.body.triptype;
    let roundtrip   = req.body.roundtrip;
    let expenses    = req.body.expenses;
    let member      = req.body.member;

    let editErrorResult = {
        "message": "Trip does not exist",
        "status" : false
    }
    let editSuccessResult = {
        "message": "Success",
        "status" : true
    }
    if (edittype === "0"){
        var idWaypointList=[]; 
        var waypointArr = waypoint;
        waypointArr = waypointArr.replace(/'/g, '"');
        waypointArr = JSON.parse(waypointArr);
        for (var i = 0; i < waypointArr.length; i++) {
            var objWaypoint = waypointArr[i];
            admin.firestore().collection('waypoints').add(objWaypoint).then((snapshot) => {
                var snapshotId = snapshot.id;
                admin.firestore().collection('waypoints').doc(snapshotId).update({
                    "id" : snapshotId,
                })
                idWaypointList.push(snapshotId);
                return "TODO";
            }).catch((error) => {
                console.log("Transaction failed: ", error);
                res.status(200).send("Failed!");
            });
        }

        var sfDocRef = admin.firestore().collection("trips").doc(tripid);

        admin.firestore().runTransaction(transaction => {
            return transaction.get(sfDocRef).then(snapshots => {
                const newWaypoint = snapshots.get('waypoints');

                for (var j=0; j < idWaypointList.length;j++){
                    newWaypoint[idWaypointList[j]] = true;
                    transaction.update(sfDocRef, 'waypoints', newWaypoint);
                }
               
                return res.status(200).json(editSuccessResult);
            });
        }).then(() => {
            let editResult = {
                "message": "An error occurred",
                "status" : false
            }
            return res.status(200).json(editResult);
        }).catch((error) => {
            return res.status(200).json(editErrorResult);
        });
    }else
    if (edittype === "1"){
         admin.firestore().collection('trips').doc(tripid).update({
            "favourite": favourite
        }).then(() => {
            return res.status(200).json(editSuccessResult);
        }).catch((error) => {
            return res.status(200).json(editErrorResult);
        });
    }else
    if (edittype === "2"){
         admin.firestore().collection('trips').doc(tripid).update({
            "image": image
        }).then(() => {
            return res.status(200).json(editSuccessResult);
        }).catch((error) => {
            return res.status(200).json(editErrorResult);
        });
    }else
    if (edittype === "3"){
        admin.firestore().collection('trips').doc(tripid).update({
            "ccid"        : ccid,
            "description" : description,
            "startdate"   : startdate,
            "enddate"     : enddate,
            "starttime"   : starttime,
            "endtime"     : endtime,
            "triptype"    : triptype,
            "roundtrip"   : roundtrip
        }).then(() => {
            return res.status(200).json(editSuccessResult);
        }).catch((error) => {
            return res.status(200).json(editErrorResult);
        });
    }
    else if (edittype === "4"){
        admin.firestore().collection('trips').doc(tripid).update({
           "expenses": expenses
       }).then(() => {
           return res.status(200).json(editSuccessResult);
       }).catch((error) => {
           return res.status(200).json(editErrorResult);
       });
    } else if (edittype === "5"){
        admin.firestore().collection('trips').doc(tripid).update({
           "member": member
       }).then(() => {
           return res.status(200).json(editSuccessResult);
       }).catch((error) => {
           return res.status(200).json(editErrorResult);
       });
   }else{
        let editResult = {
            "message": "An error occurred",
            "status" : false
        }
        return res.status(200).json(editResult);
    }
})

//GET api/removetrip
//DELETE_TRIP
app.get('/removetrip', (req, res) => {
    var userid = req.query.userid;
    if (!userid) {
        res.status(200).send("Enter userid.");
        return;
    }
    var tripid = req.query.tripid;
    if (!tripid) {
        res.status(200).send("Enter tripid.");
        return;
    }
   
    var deleteResult = {
        "message": "Success",
        "status" : true
    }
    const FieldValue = require('firebase-admin').FieldValue;

    admin.firestore().collection('trips').doc(tripid).get().then((tripDetail) => {
            if (userid !== tripDetail.data().ccid){
                admin.firestore().collection('trips').doc(tripid).update({
                    ['member.' + userid]: admin.firestore.FieldValue.delete()
                }).then(function() {
                    return res.status(200).json(deleteResult);
                }).catch((error) => {
                    let result = {
                        "message": "Trip does not exist",
                        "status" : false
                    }
                    return res.status(200).json(result);
                });
                admin.firestore().collection('members').doc(userid).delete();
            }else if (userid === tripDetail.data().ccid){
                admin.firestore().collection('trips').doc(tripid).delete();
                return res.status(200).json(deleteResult);
            }
        return "TODO";
    }).catch((error) => {
        let result = {
            "message": "Trip does not exist",
            "status" : false
        }
        return res.status(200).json(result);
    });
})

//API like trip in post
app.post('/likeTrip', (req, res) => {
    let userid = req.body.userid;
    let tripid = req.body.tripid;

    var trip = {};
    return admin.firestore().collection('trips').doc(tripid).get().then((tripDetail) => {
        let likes = tripDetail.get('likes');
        let likesCount = tripDetail.get('likesCount');
        
        let index = likes.indexOf(userid);
        if (index !== -1) {
            likes.splice(index,1);
            likesCount = parseInt(likesCount) - 1;
        } else {
            likes.push(userid);
            likesCount = parseInt(likesCount) + 1;
        }

        trip = tripDetail.data();
        trip.likes = likes;
        trip.likesCount = likesCount;

        admin.firestore().collection('trips').doc(tripid).update({
            "likesCount": likesCount,
            "likes": likes
        }).then(() => {
            let result = {
                "message": "Success",
                "status": true,
                "trip": trip
            }
            return res.status(200).json(result);
        }).catch(err => {
            console.log('Error', err);
            return res.status(200).send("Error update");
        });
        return "TODO";
    }).catch(err => {
        console.log('Error', err);
        return res.status(200).send("Error");
    });
})

//API comment trip in post
app.post('/commentTrip', (req, res) => {
    let userid  = req.body.userid;
    let tripid  = req.body.tripid;
    let content = req.body.content;

    var sfDocRef = admin.firestore().collection("trips").doc(tripid);
    var trip = {};
    //insert comment
    admin.firestore().runTransaction(transaction => {
        
        return transaction.get(sfDocRef).then(snapshots => {
            // const commentsCount = snapshots._fieldsProto.commentsCount.integerValue;
            const commentsCount = snapshots.get('commentsCount');
            const comments = snapshots.get('comments');
            const usersComment = snapshots.get('usersComment');
            comments.push(content);
            usersComment.push(userid);

            trip = snapshots.data();

            transaction.update(sfDocRef, {'comments': comments, 'usersComment': usersComment, 'commentsCount': commentsCount + 1});

            trip.comments = comments;
            trip.usersComment = usersComment;
            trip.commentsCount = commentsCount + 1;

            return "TODO";
        });
    }).then(() => {
        console.log("Transaction successfully committed!");
        let result = {
            "message": "Success",
            "status": true,
            "post": trip
        }
        return res.status(200).json(result);
    }).catch((error) => {
        console.log("Transaction failed: ", error);
        res.status(200).send("Failed!");
    });
});

//API like trip in post
app.post('/viewTrip', (req, res) => {
    let tripid = req.body.tripid;

    var trip = {};
    return admin.firestore().collection('trips').doc(tripid).get().then((tripDetail) => {
        trip = tripDetail.data();
        trip.viewsCount += 1;
        // let viewsCount = tripDetail.get('viewsCount');
        admin.firestore().collection('trips').doc(tripid).update({"viewsCount": trip.viewsCount}).then(() => {
            let result = {
                "message": "Success",
                "status": true,
                "trip": trip
            }
            return res.status(200).json(result);
        }).catch(err => {
            console.log('Error', err);
            return res.status(200).send("Error update");
        });
        return "TODO";
    }).catch(err => {
        console.log('Error', err);
        return res.status(200).send("Error");
    });
})

//POST api/waypointdelete
//DELETE_WAYPOINT
app.post('/waypointdelete', (req, res) => {
    var ccid       = req.body.ccid;
    var tripid     = req.body.tripid;
    var waypointid = req.body.waypointid;
    var deleteResult = {
        "message": "Success",
        "status" : true
    }

    const FieldValue = require('firebase-admin').FieldValue;
    return admin.firestore().collection('trips').doc(tripid).get().then((tripDetail) => {
        if (ccid !== tripDetail.data().ccid){
            let result = {
                "message": "You have no permission delete",
                "status" : false
            }
            return res.status(200).json(result);
        }else{
            admin.firestore().collection('trips').doc(tripid).update({
                ['waypoint.' + waypointid]: admin.firestore.FieldValue.delete()
            }).then(function() {
                return res.status(200).json(deleteResult);
            }).catch((error) => {
                let result = {
                    "message": "Trip does not exist",
                    "status" : false
                }
                return res.status(200).json(result);
            });
            admin.firestore().collection('waypoints').doc(waypointid).delete();
        }
        return "TODO";
    }).catch((error) => {
        let result = {
            "message": "Trip does not exist",
            "status" : false
        }
        return res.status(200).json(result);
    });
})

//GET api/getregisteredfriends
//GETFRIENDS_ENDPOINT

app.post('/getregisteredfriends', (req, res) => {
    var phones = req.body.phones;
    var emails = req.body.emails;
    // var emailList = emails.split(', ');
    let registerList=[];
    for (var i=0; i< phones.length; i++){
        registerList.push(admin.firestore().collection('users').where('phone','==',phones[i]));
    }
    return Promise.all(registerList).then(userResult => {
        var i = 0;
        var memberList = [];
        var user = {};
        userResult.forEach((members) => {
            members.forEach(function(member) {
                user = member.data();
                delete user.email;
                delete user.password;
                memberList.push(user);
            });
        });
        return res.status(200).send(memberList);
    }).catch((error) => {
        let result = {
            "message": "An error occurred",
            "status" : false
        }
        return res.status(200).json(result);
    });
})

//GET api/userlocation
//UPDATE_USER_LOCATION
app.post('/userlocation', (req, res) => {
    let userid = req.body.userid;
    let lat = req.body.lat;
    let lng = req.body.lng;
    
    var userLocationResult = {
        "message": "success",
        "status" : true,
        "lat"    : lat,
        "lng"    : lng
    }

    return admin.firestore().collection('users').doc(userid).update({
        "lat": lat,
        "lng": lng
    }).then(() => {
        return res.status(200).json(userLocationResult);
    }).catch((error) => {
        let result = {
            "message": "An error occurred",
            "status" : false
        }
        return res.status(200).json(result);
    });
})

// POST /api/createpost
// Create post
app.post('/createpost', (req, res) => {
    const busboy = new Busboy({ headers: req.headers });
    const tmpdir = os.tmpdir();

    const fields = {};
    const uploads = {};

    busboy.on('field', (fieldname, val) => {
      fields[fieldname] = val;
    });

    busboy.on('file', (fieldname, file, filename) => {
      const filepath = path.join(tmpdir, filename);
      uploads[fieldname] = filepath;
      file.pipe(fs.createWriteStream(filepath));
    });

    busboy.on('finish', () => {
      for (const name in uploads) {
        const file = uploads[name];
        //fs.unlinkSync(file);
      }
      res.send();
    });
    req.pipe(busboy);
})

//API count view in post
app.post('/viewsCount', (req, res) => {
    let postid = req.body.postid;

    return admin.firestore().collection('posts').doc(postid).get().then((postDetail) => {
        let viewsCount = postDetail._fieldsProto.viewsCount.integerValue;

        admin.firestore().collection('posts').doc(postid).update({
            "viewsCount": parseInt(viewsCount) + 1,
        }).then(() => {
            return res.status(200).send("OK!");
        }).catch(err => {
            console.log('Error', err);
            return res.status(200).send("Error update");
        });
        return "TODO";
    }).catch(err => {
        console.log('Error', err);
        return res.status(200).send("Error");
    });
})

//API like in post
app.post('/like', (req, res) => {
    let userid = req.body.userid;
    let postid = req.body.postid;

    var post = {};
    return admin.firestore().collection('posts').doc(postid).get().then((postDetail) => {
        let likes = postDetail.get('likes');
        let likesCount = postDetail.get('likesCount');
        
        let index = likes.indexOf(userid);
        if (index !== -1) {
            likes.splice(index,1);
            likesCount = parseInt(likesCount) - 1;
        } else {
            likes.push(userid);
            likesCount = parseInt(likesCount) + 1;
        }

        post = postDetail.data();
        post.likes = likes;
        post.likesCount = likesCount;

        admin.firestore().collection('posts').doc(postid).update({
            "likesCount": likesCount,
            "likes": likes
        }).then(() => {
            let result = {
                "message": "Success",
                "status": true,
                "post": post
            }
            return res.status(200).json(result);
        }).catch(err => {
            console.log('Error', err);
            return res.status(200).send("Error update");
        });
        return "TODO";
    }).catch(err => {
        console.log('Error', err);
        return res.status(200).send("Error");
    });
})

//API count comment in post
app.post('/comment', (req, res) => {
    let userid  = req.body.userid;
    let postid  = req.body.postid;
    let content = req.body.content;

    var sfDocRef = admin.firestore().collection("posts").doc(postid);
    var post = {};
    //insert comment
    admin.firestore().runTransaction(transaction => {
        
        return transaction.get(sfDocRef).then(snapshots => {
            // const commentsCount = snapshots._fieldsProto.commentsCount.integerValue;
            const commentsCount = snapshots.get('commentsCount');
            const comments = snapshots.get('comments');
            const usersComment = snapshots.get('usersComment');
            comments.push(content);
            usersComment.push(userid);

            post = snapshots.data();

            transaction.update(sfDocRef, {'comments': comments, 'usersComment': usersComment, 'commentsCount': commentsCount + 1});

            post.comments = comments;
            post.usersComment = usersComment;
            post.commentsCount = commentsCount + 1;

            return "TODO";
        });
    }).then(() => {
        console.log("Transaction successfully committed!");
        let result = {
            "message": "Success",
            "status": true,
            "post": post
        }
        return res.status(200).json(result);
    }).catch((error) => {
        console.log("Transaction failed: ", error);
        res.status(200).send("Failed!");
    });
});

//API like trip in post
app.post('/viewPost', (req, res) => {
    let postid = req.body.postid;

    var post = {};
    return admin.firestore().collection('posts').doc(postid).get().then((postDetail) => {
        post = postDetail.data();
        post.viewsCount += 1;
        admin.firestore().collection('posts').doc(postid).update({"viewsCount": post.viewsCount}).then(() => {
            let result = {
                "message": "Success",
                "status": true,
                "post": post
            }
            return res.status(200).json(result);
        }).catch(err => {
            console.log('Error', err);
            return res.status(200).send("Error update");
        });
        return "TODO";
    }).catch(err => {
        console.log('Error', err);
        return res.status(200).send("Error");
    });
})

//API create a new post in post
app.post('/createNewPost', (req, res) => {
    // let postid = req.body.postid;
    let userid = req.body.userid;
    let tripid = req.body.tripid;
    let videoLink = req.body.videoLink;
    let imageLink = req.body.imageLink;
    let title = req.body.title;
    let contents = req.body.contents;
    // let createdTime = req.body.createdTime;
    let status = req.body.status;
    let usersComment = [];
    let comments = [];
    let likes = [];

    let datePattern = 'YYYY-MM-DD HH:mm:ss';

    var newPost = {
        // "postid": postid,
        "userid": userid,
        "tripid": tripid,
        "videoLink": videoLink,
        "imageLink": imageLink,
        "title": title,
        "contents": contents,
        "createdTime": new Date().getTime(),
        "status": status,
        "comments": comments,
        "usersComment": usersComment,
        "likes": likes,
        "viewsCount": 0,
        "likesCount": 0,
        "commentsCount": 0
    };

    //insert new post
    admin.firestore().collection('posts').add(newPost).then(snapshot => {
        let result = {
            "message": "Success",
            "postid": snapshot.id,
            "status": true
        }
        var sfDocRef = admin.firestore().collection("posts").doc(snapshot.id);
        admin.firestore().runTransaction(transaction => {
            return transaction.get(sfDocRef).then(snapshots => {
                // update tripId
                transaction.update(sfDocRef, 'postid', snapshot.id);
                return "TODO";
            });
        }).then(() => {
            return res.status(200).send("Transaction successfully committed!")
        }).catch((error) => {
            console.log("Transaction failed: ", error);
        });
        return res.status(200).json(result);
    }).catch((error) => {
        console.log("An error occurred: ", error);
        let result = {
            "message": "An error occurred",
            "status" : false
        }
        return res.status(200).json(result);
    });
});

//API create a new post in post
// app.post('/createNewPost', (req, res) => {
//     // let postid = req.body.postid;
//     let userid = req.body.userid;
//     let tripid = req.body.tripid;
//     let videoLink = req.body.videoLink;
//     let imageLink = req.body.imageLink;
//     let title = req.body.title;
//     let contents = req.body.contents;
//     let createdTime = req.body.createdTime;
//     let status = req.body.status;
//     let usersComment = [];
//     let comments = [];
//     let likes = [];

//     var newPost = {
//         // "postid": postid,
//         "userid": userid,
//         "tripid": tripid,
//         "videoLink": videoLink,
//         "imageLink": imageLink,
//         "title": title,
//         "contents": contents,
//         "createdTime": createdTime,
//         "status": status,
//         "comments": comments,
//         "usersComment": usersComment,
//         "likes": likes,
//         "viewsCount": 0,
//         "likesCount": 0,
//         "commentsCount": 0
//     };

//     //insert new post
//     admin.firestore().collection('posts').add(newPost).then(snapshot => {
//         let result = {
//             "message": "Success",
//             "postid": snapshot.id,
//             "status": true
//         }
//         var sfDocRef = admin.firestore().collection("posts").doc(snapshot.id);
//         admin.firestore().runTransaction(transaction => {
//             return transaction.get(sfDocRef).then(snapshots => {
//                 // update tripId
//                 transaction.update(sfDocRef, 'postid', snapshot.id);
//                 return "TODO";
//             });
//         }).then(() => {
//             return res.status(200).send("Transaction successfully committed!")
//         }).catch((error) => {
//             console.log("Transaction failed: ", error);
//         });
//         return res.status(200).json(result);
//     }).catch((error) => {
//         console.log("An error occurred: ", error);
//         let result = {
//             "message": "An error occurred",
//             "status" : false
//         }
//         return res.status(200).json(result);
//     });
// });

//API user's activity in post
app.get('/getUserActivity', (req, res) => {
    let userid = req.query.userid;
    let page = parseInt(req.query.page);

    let userActivity=[];
    userActivity.push(admin.firestore().collection('posts').where('likes','array-contains',userid).orderBy('createdTime','desc').get());
    userActivity.push(admin.firestore().collection('posts').where('usersComment','array-contains',userid).orderBy('createdTime','desc').get());

    userActivity.push(admin.firestore().collection('trips').where('likes','array-contains',userid).orderBy('createdTime','desc').get());
    userActivity.push(admin.firestore().collection('trips').where('usersComment','array-contains',userid).orderBy('createdTime','desc').get());

    return Promise.all(userActivity).then((results) => {
        var postsActivity = [];
        var tripsActivity = {};
        var mapPostid = {};
        var mapTripid = {};
        let index = 0;
        results.forEach((data) => {
            // posts result
            if (!data.empty) {
                if (index < 2) {
                    data.forEach(docs => {
                        let post = docs.data();
                        post.type = 'post';
                        if (post.likes !== undefined && post.likes.indexOf(userid) !== -1)
                            post.liked = true;
                        if (post.usersComment !== undefined && post.usersComment.indexOf(userid) !== -1)
                            post.commented = true;

                        mapTripid[post.tripid] = post.tripid;
                        if (mapPostid[post.postid] === undefined) {
                            mapPostid[post.postid] = post.postid;
                            postsActivity.push(post);
                        }
                    });
                } else {
                    data.forEach(docs => {
                        let trip = docs.data();
                        if (trip.likes !== undefined && trip.likes.indexOf(userid) !== -1)
                            trip.liked = true;
                        if (trip.usersComment !== undefined && trip.usersComment.indexOf(userid) !== -1)
                            trip.commented = true;
                        tripsActivity[docs.data().tripid] = trip;
                        tripsActivity[docs.data().tripid].type = 'trip';
                    });
                }
            }
            index++;
        });

        var tripsQuery = [];
        var tripsResult = [];
        for (var id in mapTripid) {
            if (tripsActivity[id] === undefined) 
                tripsQuery.push(admin.firestore().doc('trips/' + id));
        }
        
        tripsResult.push(admin.firestore().getAll(tripsQuery));
        
        return Promise.all(tripsResult).then(results => {
            let mapTrips = {};
            results.forEach(trips => {
                trips.forEach(trip => {
                    mapTrips[trip.data().tripid] = trip.data();
                });
            });

            for (var i = 0; i < postsActivity.length; i++) {
                if (mapTrips[postsActivity[i].tripid] !== undefined) {
                    postsActivity[i].trip = mapTrips[postsActivity[i].tripid];
                } else if (tripsActivity[postsActivity[i].tripid] !== undefined) {
                    postsActivity[i].trip = tripsActivity[postsActivity[i].tripid];
                }
            }
            tripsActivity = Object.keys(tripsActivity).map(i => tripsActivity[i]);
            var activities = postsActivity.concat(tripsActivity);
            activities = activities.sort((a,b) => (b.createdTime - a.createdTime));
            var totalPage = Math.floor(activities.size/page) + 1;
            let result = {
                "message": "Sucess",
                "status" : true,
                "data"   : activities,
                "totalpage": totalPage,
                "page": page,
                "totalitems": activities.length,
            }
            return res.status(200).json(result);
        });
    }).catch(error => {
        let result = {
            "message": "An error occurred",
            "status" : false
        }
        console.log(error);
        return res.status(200).json(result);
    });
})

//API get friends feed in post
app.post('/getFriendActivity', (req, res) => {
    let phones = req.body.phones;
    let page = parseInt(req.body.page, 10);
    let userid = req.body.userid;

    let friendList=[];
    for (var i=0; i< phones.length; i++) {
        friendList.push(admin.firestore().collection('users').where('phone','==',phones[i]).get());
    }
    var friendListId = [];
    Promise.all(friendList).then(usersResult => {
        var i = 0;
        usersResult.forEach((members) => {
            members.forEach(function(member) {
                friendListId.push(member.data().id);
            });
        });

        let friendsActivity=[];
        let friendsNumber = friendListId.length;
        // find in posts
        for(var j = 0; j < friendsNumber; j++) {
            friendsActivity.push(admin.firestore().collection('posts').where('likes','array-contains',friendListId[j]).orderBy('createdTime','desc').get());
            friendsActivity.push(admin.firestore().collection('posts').where('usersComment','array-contains',friendListId[j]).orderBy('createdTime','desc').get());
        }

        // find in trips
        for(j = 0; j < friendsNumber; j++) {
            friendsActivity.push(admin.firestore().collection('trips').where('likes','array-contains',friendListId[j]).orderBy('createdTime','desc').get());
            friendsActivity.push(admin.firestore().collection('trips').where('usersComment','array-contains',friendListId[j]).orderBy('createdTime','desc').get());
        }

        return Promise.all(friendsActivity).then((results) => {
            var postsActivity = [];
            var tripsActivity = {};
            var mapPostid = {};
            var mapTripid = {};
            let index = 0;
            results.forEach((data) => {
                // posts result
                if (!data.empty) {
                    if (index < friendsNumber*2) {
                        data.forEach(docs => {
                            let post = docs.data();
                            post.type = 'post';
                            if (post.likes !== undefined && post.likes.indexOf(userid) !== -1)
                                post.liked = true;
                            if (post.usersComment !== undefined && post.usersComment.indexOf(userid) !== -1)
                                post.commented = true;

                            mapTripid[post.tripid] = post.tripid;
                            if (mapPostid[post.postid] === undefined) {
                                mapPostid[post.postid] = post.postid;
                                postsActivity.push(post);
                            }
                        });
                    } else {
                        data.forEach(docs => {
                            let trip = docs.data();
                            if (trip.likes !== undefined && trip.likes.indexOf(userid) !== -1)
                                trip.liked = true;
                            if (trip.usersComment !== undefined && trip.usersComment.indexOf(userid) !== -1)
                                trip.commented = true;
                            tripsActivity[docs.data().tripid] = trip;
                            tripsActivity[docs.data().tripid].type = 'trip';
                        });
                    }
                }
                index++;
            });

            var tripsQuery = [];
            var tripsResult = [];
            for (var id in mapTripid) {
                if (tripsActivity[id] === undefined) 
                    tripsQuery.push(admin.firestore().doc('trips/' + id));
            }
           
            tripsResult.push(admin.firestore().getAll(tripsQuery));
            
            return Promise.all(tripsResult).then(results => {
                let mapTrips = {};
                results.forEach(trips => {
                    trips.forEach(trip => {
                        mapTrips[trip.data().tripid] = trip.data();
                    });
                });

                for (var i = 0; i < postsActivity.length; i++) {
                    if (mapTrips[postsActivity[i].tripid] !== undefined) {
                        postsActivity[i].trip = mapTrips[postsActivity[i].tripid];
                    } else if (tripsActivity[postsActivity[i].tripid] !== undefined) {
                        postsActivity[i].trip = tripsActivity[postsActivity[i].tripid];
                    }
                }
                tripsActivity = Object.keys(tripsActivity).map(i => tripsActivity[i]);
                var activities = postsActivity.concat(tripsActivity);
                activities = activities.sort((a,b) => (b.createdTime - a.createdTime));
                var totalPage = Math.floor(activities.size/page) + 1;
                let result = {
                    "message": "Sucess",
                    "status" : true,
                    "data"   : activities,
                    "totalpage": totalPage,
                    "page": page,
                    "totalitems": activities.length,
                }
                return res.status(200).json(result);
            });
            
        }).catch(error => {
            let result = {
                "message": "An error occurred",
                "status" : false
            }
            console.log(error);
            return res.status(200).json(result);
        });

    }).catch((error) => {
        let result = {
            "message": "An error occurred",
            "status" : false
        }
        console.log(error);
        return res.status(200).json(result);
    });
})

//API update follow user in post
app.post('/updateFollow', (req, res) => {
    let userid = req.body.userid;
    let followId = req.body.followId;
    let option = req.body.option; 
    // let type = req.body.type; 

    // if (type !== 'follower' && type !== 'following' && type !== 'block') {
    //     console.log("Error /updateFollow: Invalid type");
    //     res.status(200).send("Failed!");
    // }
    if (option !== 'add' && option !== 'remove' && option !== 'block') {
        console.log("Error /updateFollow: Invalid option");
        res.status(200).send("Failed!");
    }
    if ('' === followId || undefined === followId) {
        console.log("Error /updateFollow: Invalid followId");
        res.status(200).send("Failed!");
    } 

    // Create a reference to the SF doc.
    var sfDocRef = admin.firestore().collection("users").doc(userid);
    var followDocRef = admin.firestore().collection("users").doc(followId);
    var user = null;
    admin.firestore().runTransaction(transaction => {
        return transaction.get(sfDocRef).then(snapshot => {
            user = snapshot.data();
            delete user.email;
            delete user.password;

            if (option !== 'block') {
                return transaction.get(sfDocRef).then(snapshot => {
                    return transaction.get(followDocRef).then(follower => {
                        let userFollows = snapshot.get('follower');

                        let followerFollows = follower.get('following');
                        let followerBlock = follower.get('block');
    
                        const index = userFollows.indexOf(followId);
                        const indexFollow = followerFollows.indexOf(userid);
                        const indexBlock = followerFollows.indexOf(userid);
                        switch(option) {
                            case 'add':
                                if (index === -1 && indexBlock === -1) { 
                                    userFollows.push(followId);
                                    transaction.update(sfDocRef, 'following', userFollows);
                                }
                                if (indexFollow === -1) {
                                    followerFollows.push(userid);
                                    transaction.update(followDocRef, 'follower', followerFollows);
                                }
                                break;
                            case 'remove':
                                if (index !== -1) {
                                    userFollows.splice(index, 1);
                                    transaction.update(sfDocRef, 'following', userFollows);
                                }
                                if (indexFollow !== -1) {
                                    followerFollows.splice(indexFollow, 1);
                                    transaction.update(followDocRef, 'follower', followerFollows);
                                }
                                break;
                            default:
                                break;
                        }
                        user.follower = userFollows;
                        return "TODO";
                    });
                });
            } else {
                return transaction.get(followDocRef).then(following => {
                    let blocks = snapshot.get('block');
                    let follows = following.get('following');

                    const index = blocks.indexOf(followId);
                    const followIndex = follows.indexOf(userid);
                    if (index === -1) {
                        blocks.push(followId);
                        user.block = blocks;
                        transaction.update(sfDocRef, 'block', blocks);
                    }      
                    if (followIndex === -1) {
                        follows.splice(followIndex, 1);
                        transaction.update(followDocRef, 'following', follows);
                    }          
                    return "TODO";
                });
            }
        });
    }).then(() => {
        console.log("Transaction successfully committed!");
        var result = {};
        result.message = "success";
        result.status = true;
        result.user = user;
        return res.status(200).json(result);
    }).catch((error) => {
        console.log("Transaction failed: ", error);
        res.status(200).send("Failed!");
    });
})

//API get follow user in get
app.get('/getFollow', (req, res) => {
    let userid = req.query.userid;
    let type = req.query.type; 
    if (type !== 'follower' && type !== 'following' && type !== 'block') {
        return;
    }
    return admin.firestore().collection("users").doc(userid).get().then(snapshot => {
        let followList = snapshot.get(type);
        var followsQuery = [];
        for (var i = 0; i < followList.length; i++) {
            followsQuery.push(admin.firestore().collection("users").where('id', '==', followList[i]).get());
        }

        return Promise.all(followsQuery).then(results => {

            var usersFollow = [];
            results.forEach((members) => {
                var memInfo;
                members.forEach(function(member) {
                    memInfo = member.data();
                    delete memInfo.email;
                    delete memInfo.password;
                    usersFollow.push(memInfo);
                });
            });

            let result = {
                "message": "Sucess",
                "status" : true,
                "data"   : usersFollow,
                "totalitems": usersFollow.length,
            }
            return res.status(200).json(result);
        }).catch((error) => {
            console.log("Transaction failed: ", error);
            res.status(200).send("Failed!");
        });

    }).catch((error) => {
        console.log("Transaction failed: ", error);
        res.status(200).send("Failed!");
    });
})

//API update member wallet in post
app.post('/updateMemberWallet', (req, res) => {
    let userid = req.body.userid;
    let tripid = req.body.tripid;
    let paid = req.body.paid; 

    // Create a reference to the SF doc.
    var sfDocRef = admin.firestore().collection("trips").doc(tripid);

    admin.firestore().runTransaction(transaction => {
        return transaction.get(sfDocRef).then(snapshot => {
            let memWallet = snapshot.get('memWallet');
            let total_paid = snapshot.get('total_paid');
            if (memWallet[userid] !== undefined) {
                memWallet[userid].paid = memWallet[userid].paid + paid;
                transaction.update(sfDocRef, {'memWallet': memWallet, 'total_paid': total_paid + paid});
            } else {
                console.log("Error api updateMemberWallet. No member id: " + userid);
            }
            return "TODO";
        });
    }).then(() => {
        console.log("Transaction successfully committed!");
        var result = {};
        result.message = "success";
        result.status = true;
        return res.status(200).json(result);
    }).catch((error) => {
        console.log("Transaction failed: ", error);
        res.status(200).send("Failed!");
    });
})

//API update expenses wallet in post
app.post('/updateExpensesTrip', (req, res) => {
    let tripid = req.body.tripid;
    let name = req.body.name;
    let desc = req.body.desc; 
    let cost = parseFloat(req.body.cost);
    let type = req.body.type;

    // Create a reference to the SF doc.
    var sfDocRef = admin.firestore().collection("trips").doc(tripid);

    admin.firestore().runTransaction(transaction => {
        return transaction.get(sfDocRef).then(snapshot => {
            let expenses = snapshot.get('expenses');
            let total_cost = snapshot.get('total_cost') === undefined ? 0 : parseFloat(snapshot.get('total_cost'));
            let memWallet = snapshot.get('memWallet');
            let numberOfMem = Object.keys(memWallet).length;
            // Add expenses
            if ("1" === type) {
                if (numberOfMem > 0) {
                    var costPerMemberAdd = cost/numberOfMem;
                    for (var userid in memWallet) {
                        if (memWallet[userid] !== undefined && memWallet[userid].cost !== undefined) 
                        memWallet[userid].cost += costPerMemberAdd;
                    }
                }
                expenses[name] = {
                    'name': name,
                    'desc': desc,
                    'cost': cost
                };
                transaction.update(sfDocRef, {"expenses": expenses, "total_cost": (total_cost + cost), 'memWallet': memWallet});
            // Delete expenses
            } else if ("2" === type) {
                let expenseDel = expenses[name];
                if (expenseDel !== undefined) {
                    if (numberOfMem > 0) {
                        var substractVal = expenseDel.cost/numberOfMem;
                        for (userid in memWallet) {
                            if (memWallet[userid] !== undefined && memWallet[userid].cost !== undefined) 
                            memWallet[userid].cost -= (substractVal > membmemWalleters[userid].cost ? memWallet[userid].cost : substractVal);
                        }
                    }
                    delete expenses[name];
                }
                transaction.update(sfDocRef, {'expenses': expenses, 'total_cost': (total_cost - expenseDel.cost), 'memWallet': memWallet});
            // Update expenses
            } else if ("3" === type) {
                let expenseUpdate = expenses[name];
                if (expenseUpdate !== undefined) {
                    let changeVal = cost - expenseUpdate.cost;
                    expenseUpdate.desc = desc;
                    expenseUpdate.cost = cost;
                    total_cost = total_cost + changeVal;
                    if (numberOfMem > 0) {
                        var costPerMemberUpdate = changeVal/numberOfMem;
                        for (userid in memWallet) {
                            if (memWallet[userid] !== undefined && memWallet[userid].cost !== undefined) 
                            memWallet[userid].cost += costPerMemberUpdate;
                        }
                    }
                }
                expenses[name] = {
                    'name': name,
                    'desc': desc,
                    'cost': cost
                };
                transaction.update(sfDocRef, {'expenses': expenses, 'total_cost': total_cost, 'memWallet': memWallet});
            }
            return "TODO";
        });
        
    }).then(() => {
        console.log("Transaction successfully committed!");
        var result = {};
        result.message = "success";
        result.status = true;
        return res.status(200).json(result);
    }).catch((error) => {
        console.log("Transaction failed: ", error);
        res.status(200).send("Failed!");
    });
})

//API update expenses wallet in post
app.get('/getTripCommunity', (req, res) => {
    let type = req.query.type; 
    let userid = req.query.userid;

    // let page = parseInt(req.query.page);
    let orderVal = '';
    if ('1' === type) {
        orderVal = 'commentsCount';
    } else if ('2' === type) {
        orderVal = 'likesCount';
    } else if ('3' === type) {
        orderVal = 'createdTime';
    }
    
    return admin.firestore().collection('trips').where('status','==','public').orderBy(orderVal,'desc').get().then(tripsDetail => {
        
        var trips = [];
        var destinations = {};
        tripsDetail.forEach(data => {
            let trip = data.data();
            if (trip.likes !== undefined && trip.likes.indexOf(userid) !== -1)
                trip.liked = true;
            if (trip.usersComment !== undefined && trip.usersComment.indexOf(userid) !== -1)
                trip.commented = true;

            trips.push(trip);
            if (destinations[trip.endpoint] === undefined) {
                destinations[trip.endpoint] = {
                    destination: trip.endpoint,
                    count: 1,
                    tripids: [trip.tripid],
                    createdTime: ''
                };
                if ('3' === type) {
                    destinations[trip.endpoint].createdTime = trip.createdTime;
                }
            } else {
                destinations[trip.endpoint].count += 1;
                if (destinations[trip.endpoint].tripids.indexOf(trip.tripid) === -1) {
                    destinations[trip.endpoint].tripids.push(trip.tripid);
                }
            }
        });
        destinations = Object.keys(destinations).map(i => destinations[i]).sort((a,b) => ('3' === type) ? (b.createdTime - a.createdTime) : (b.count - a.count));
        
        var result = {};
        result.message = "success";
        result.status = true;
        result.trips = {
            data: trips,
            size: tripsDetail.size
        }
        result.destination = {
            data: destinations,
            size: destinations.length
        }

        return res.status(200).send(result);
    }).catch((error) => {
        console.log("Transaction failed: ", error);
        res.status(200).send("Failed!");
    });
})

//API get post community in post
app.get('/getPostCommunity', (req, res) => {
    let type = req.query.type; 
    // let page = parseInt(req.query.page);
    let orderVal = '';
    if ('1' === type) {
        orderVal = 'commentsCount';
    } else if ('2' === type) {
        orderVal = 'likesCount';
    } else if ('3' === type) {
        orderVal = 'createdTime';
    }
    
    return admin.firestore().collection('posts').where('status','==','true').orderBy(orderVal,'desc').get().then(postsDetail => {
        
        var posts = [];
        postsDetail.forEach(data => {
            posts.push(data.data());
        });
        var result = {};
        result.message = "success";
        result.status = true;
        result.posts = posts;
        result.size = postsDetail.size;

        return res.status(200).send(result);
    }).catch((error) => {
        console.log("Transaction failed: ", error);
        res.status(200).send("Failed!");
    });
})

//API update expenses wallet in post
app.get('/getDestinations', (req, res) => {
    return admin.firestore().collection('trips').orderBy('createdTime','desc').get().then(tripsDetail => {
        var trips = [];
        var destinations = {};
        var trip = {};
        tripsDetail.forEach(data => {

            trip = data.data();
            delete trip.comments;
            delete trip.likes;
            delete trip.usersComment;
            delete trip.usersComment;

            if (destinations[trip.endpoint] === undefined) {
                
                destinations[trip.endpoint] = {
                    destination: trip.endpoint,
                    count: 1,
                    tripids: [trip.tripid],
                    trips: [trip]
                };
            } else {
                destinations[trip.endpoint].count += 1;
                if (destinations[trip.endpoint].tripids.indexOf(trip.tripid) === -1) {
                    destinations[trip.endpoint].tripids.push(trip.tripid);
                    destinations[trip.endpoint].trips.push(trip);
                }
            }
        });

        destinations = Object.keys(destinations).map(i => destinations[i]);
        
        var result = {};
        result.message = "success";
        result.status = true;
        result.destination = destinations;
        result.size = destinations.length;

        return res.status(200).json(result);
    }).catch((error) => {
        console.log("Transaction failed: ", error);
        res.status(200).send("Failed");
    });
})

//API search in post
app.get('/searchData', (req, res) => {
    let type = req.query.type;
    let userid = req.query.userid;
    if (type === undefined) {
        return res.status(200).send("Failed");
    }
    let result = {};
    result.message = "success";
    result.status = true;

    // get all trips
    if ('1' === type) {
        return admin.firestore().collection('trips').orderBy('createdTime','desc').get().then(tripsDetail => {
            let trips = [];
            let trip = {};
            tripsDetail.forEach(data => {
                trip = data.data();
                if (trip.likes !== undefined && trip.likes.indexOf(userid) !== -1) 
                    trip.liked = true;
                if (trip.usersComment !== undefined && trip.usersComment.indexOf(userid) !== -1)
                    trip.commented = true;
                trips.push(trip);
            });
            result.data = trips;
            result.type = 'trip';
            return res.status(200).json(result);

        }).catch((error) => {
            console.log("Transaction failed: ", error);
            res.status(200).send("Failed");
        });
    // get all post
    } else if ('2' === type) {
        return admin.firestore().collection('posts').orderBy('createdTime','desc').get().then(postsDetail => {
            let posts = [];
            let post = {};
            postsDetail.forEach(data => {
                post = data.data();
                if (post.likes !== undefined && post.likes.indexOf(userid) !== -1)
                    post.liked = true;
                if (post.usersComment !== undefined && post.usersComment.indexOf(userid) !== -1)
                    post.commented = true;
                posts.push(post);
            });
            result.data = posts;
            result.type = 'post';
            return res.status(200).json(result);
        }).catch((error) => {
            console.log("Transaction failed: ", error);
            res.status(200).send("Failed");
        });
    // get all user
    } else if ('3' === type) {
        return admin.firestore().collection('users').get().then(postsDetail => {
            let users = [];
            let user;
            postsDetail.forEach(data => {
                user = data.data();
                delete user.email;
                delete user.password;
                users.push(data.data());
            });
            result.data = users;
            result.type = 'user';
            return res.status(200).json(result);
        }).catch((error) => {
            console.log("Transaction failed: ", error);
            res.status(200).send("Failed");
        });
    }
})


//API update member wallet in post
app.post('/createTeam', (req, res) => {
    // let postid = req.body.postid;
    let userid = req.body.userid;
    let tripid = req.body.tripid;
    let name = req.body.name;
    let members = [];

    var newTeam = {
        "userid": userid,
        "tripid": tripid,
        "name": name,
        "members": members
    };

    //insert new post
    admin.firestore().collection('teams').add(newTeam).then(snapshot => {
        let result = {
            "message": "Success",
            "teamid": snapshot.id,
            "status": true
        }
        var sfDocRef = admin.firestore().collection("teams").doc(snapshot.id);
        admin.firestore().runTransaction(transaction => {
            return transaction.get(sfDocRef).then(snapshots => {
                // update tripId
                transaction.update(sfDocRef, 'teamid', snapshot.id);
                return "TODO";
            });
        }).then(() => {
            return res.status(200).send("Transaction successfully committed!")
        }).catch((error) => {
            console.log("Transaction failed: ", error);
        });
        return res.status(200).json(result);
    }).catch((error) => {
        console.log("An error occurred: ", error);
        let result = {
            "message": "An error occurred",
            "status" : false
        }
        return res.status(200).json(result);
    });
});

// POST /api/acceptTeam
app.post('/acceptTeam', (req, res) => {
    var userid = req.body.userid;
    var teamid = req.body.teamid;

    // Create a reference to the SF doc.
    var sfDocRef = admin.firestore().collection('teams').doc(teamid);

    let teams = [];
    admin.firestore().runTransaction(transaction => {
        return transaction.get(sfDocRef).then(snapshot => {
          teams = snapshot.get('members');
          if (teams !== undefined) {
            if (teams.indexOf(userid) === -1) 
                teams.push(userid);
            else 
                teams.splice(teams.indexOf(userid));
            transaction.update(sfDocRef, 'members', teams);
          } else {
            res.status(200).send("Failed!");      
          }
          return "TODO";
        });
    }).then(() => {
        console.log("Transaction successfully committed!");
        var result = {};
        result.message = "success";
        result.status = true;
        result.teams = teams;
        return res.json({result:result});
    }).catch((error) => {
        console.log("Transaction failed: ", error);
        res.status(200).send("Failed!");
    });
});   

//API search trip by name or desc in post
app.get('/getTeams', (req, res) => {
    let tripid = req.query.tripid;
    let userid = req.query.userid;
    let type = req.query.type;
    if (tripid === undefined) {
        return res.status(200).send("Failed");
    }
    let usersId = {};
    let usersQuery = [];
    let teams = [];
    return admin.firestore().collection("teams")
            .where(
                type === '1' ? 'tripid' : 'members',
                type === '1' ? '==' : 'array-contains', 
                type === '1' ? tripid : userid)
            .get().then(teamsDetail => {
        let members = [];
        teamsDetail.forEach(team => {
            members = team.data().members;
            if (members !== undefined) {
                for (let userid in members) 
                    usersId[members[userid]] = members[userid];
            }
            teams.push(team.data());
        });
        for (let userid in usersId) 
            usersQuery.push(admin.firestore().collection("users").doc(usersId[userid]).get());
        let users = {};
        return Promise.all(usersQuery).then(results => {
            var usersFollow = [];
            results.forEach((snapshot) => {
                var memInfo;
                memInfo = snapshot.data();
                delete memInfo.email;
                delete memInfo.password;
                users[memInfo.id] = memInfo;
            });

            for (let i = 0; i < teams.length; i++) {
                let members = [];
                for (let id = 0; id < teams[i].members.length; id++) {
                    members[id] = users[teams[i].members[id]];
                }
                teams[i].members = members;
            }

            let result = {
                "message": "Sucess",
                "status" : true,
                "data"   : teams,
                "totalitems": teams.size,
            }
            return res.status(200).json(result);
        }).catch((error) => {
            console.log("Transaction failed: ", error);
            res.status(200).send("Failed!");
        });

    }).catch((error) => {
        console.log("Transaction failed: ", error);
        res.status(200).send("Failed!");
    });
})

//API search trip by name or desc in post
// app.get('/getUserTeams', (req, res) => {
//     let userid = req.query.userid;
//     if (userid === undefined) {
//         return res.status(200).send("Failed");
//     }
//     let usersId = {};
//     let usersQuery = [];
//     let teams = [];
//     return admin.firestore().collection("teams").where('members','array-contains', userid).get().then(teamsDetail => {
//         let members = [];
//         teamsDetail.forEach(team => {
//             members = team.data().teams;
//             if (members !== undefined) {
//                 for (let userid in members) 
//                     usersId[members[userid]] = members[userid];
//             }
//             teams.push(team.data());
//         });
        
//         for (let userid in usersId) 
//             usersQuery.push(admin.firestore().collection("users").doc(usersId[userid]).get());

//         let users = {};
//         return Promise.all(usersQuery).then(results => {
//             var usersFollow = [];
//             results.forEach((snapshot) => {
//                 var memInfo;
//                     memInfo = snapshot.data();
//                     delete memInfo.email;
//                     delete memInfo.password;
//                     users[memInfo.id] = memInfo;
//             });

//             for (let i = 0; i < teams.length; i++) {
//                 let members = [];
//                 for (let id = 0; id < teams[i].teams.length; id++) {
//                     members[id] = users[teams[i].teams[id]];
//                 }
//                 teams[i].members = members;
//             }

//             let result = {
//                 "message": "Sucess",
//                 "status" : true,
//                 "data"   : teams,
//                 "totalitems": teams.size,
//             }
//             return res.status(200).json(result);
//         }).catch((error) => {
//             console.log("Transaction failed: ", error);
//             res.status(200).send("Failed!");
//         });

//     }).catch((error) => {
//         console.log("Transaction failed: ", error);
//         res.status(200).send("Failed!");
//     });
// })

//API total like of user in post
app.get('/getCountUserLike', (req, res) => {
    let userid = req.query.userid;

    let userActivity=[];
    userActivity.push(admin.firestore().collection('posts').where('likes','array-contains',userid).get());
    userActivity.push(admin.firestore().collection('trips').where('likes','array-contains',userid).get());
    return Promise.all(userActivity).then((results) => {
        let counter = 0;
        results.forEach((data) => {
            if (!data.empty) 
                counter += data.size;
        });

        let result = {
            "message": "Sucess",
            "status" : true,
            "totalLike"   : counter
        }
        return res.status(200).json(result);

    }).catch(error => {
        let result = {
            "message": "An error occurred",
            "status" : false
        }
        console.log(error);
        return res.status(200).json(result);
    });
})

//API total like of user in post
app.get('/getCountDistanceUser', (req, res) => {

    let userid = req.query.userid;

    let result = {
        "message": "Sucess",
        "status" : true,
        "totalLike"   : 300
    }
    return res.status(200).json(result);

    // let userid = req.query.userid;

    // let userActivity=[];
    // userActivity.push(admin.firestore().collection('posts').where('likes','array-contains',userid).get());
    // userActivity.push(admin.firestore().collection('trips').where('likes','array-contains',userid).get());
    // return Promise.all(userActivity).then((results) => {
    //     let counter = 0;
    //     results.forEach((data) => {
    //         if (!data.empty) 
    //             counter += data.size;
    //     });

    //     let result = {
    //         "message": "Sucess",
    //         "status" : true,
    //         "totalLike"   : counter
    //     }
    //     return res.status(200).json(result);

    // }).catch(error => {
    //     let result = {
    //         "message": "An error occurred",
    //         "status" : false
    //     }
    //     console.log(error);
    //     return res.status(200).json(result);
    // });
})

//API total like of user in post
app.get('/sendResetPass', (req, res) => {
    let email = req.query.email;

    admin.firestore().collection('users').where('email', '==', email).get().then(snapshot => {
        if(snapshot.size > 0){
            let resultSignup = { 
                "message" : 'Account exists with same email',
                "status"  : false
            }
            return res.status(200).json(resultSignup);
         }
        admin.firestore().collection('users').add(newUser).then((writeResult) => {
            var userResult = {};
            userResult.id = writeResult.id
            userResult.email =  req.body.email;
            userResult.message = "success";
            userResult.status = true;
            
            admin.firestore().collection('users').doc(userResult.id).update({
                "id" : userResult.id,
            })
            return res.json(userResult);
        }).catch((error) => {
            let resultSignup = { 
                "message" : 'Error',
                "status"  : false
            }
            return res.status(200).json(resultSignup);
        });
       return "TODO";
    }).catch((error) => {
        let result = {
            "message": "An error occurred",
            "status" : false
        }
        return res.status(200).json(result);
    });
})

//API count comment in post
app.post('/userAction', (req, res) => {
    let userid  = req.body.userid;
    let postid  = req.body.postid;
    let content = req.body.content;

    var sfDocRef = admin.firestore().collection("posts").doc(postid);
    var post = {};
    //insert comment
    admin.firestore().runTransaction(transaction => {
        
        return transaction.get(sfDocRef).then(snapshots => {
            // const commentsCount = snapshots._fieldsProto.commentsCount.integerValue;
            const commentsCount = snapshots.get('commentsCount');
            const comments = snapshots.get('comments');
            const usersComment = snapshots.get('usersComment');
            comments.push(content);
            usersComment.push(userid);

            post = snapshots.data();

            transaction.update(sfDocRef, {'comments': comments, 'usersComment': usersComment, 'commentsCount': commentsCount + 1});

            post.comments = comments;
            post.usersComment = usersComment;
            post.commentsCount = commentsCount + 1;

            return "TODO";
        });
    }).then(() => {
        console.log("Transaction successfully committed!");
        let result = {
            "message": "Success",
            "status": true,
            "post": post
        }
        return res.status(200).json(result);
    }).catch((error) => {
        console.log("Transaction failed: ", error);
        res.status(200).send("Failed!");
    });
});

//API last trip join in post
app.get('/getLastTripJoin', (req, res) => {
    let userid = req.query.userid;

    let tripJoinQuery=[];
    tripJoinQuery.push(admin.firestore().collection('trips').where('members','array-contains',userid).get());

    return Promise.all(tripJoinQuery).then((results) => {
        let lasttripJoin = 0;
        let lastTrip = null;
        results.forEach((data) => {
            if (!data.empty) {
                data.forEach(docs => {
                    if (docs.get('memWallet')[userid] !== undefined) {
                        if (docs.get('memWallet')[userid].acceptTime > lasttripJoin) 
                            lastTrip = docs.data();
                    }
                });
            }
        });

        if (lastTrip !== null) {
            if (lastTrip.likes.indexOf(userid) !== -1)
                lastTrip.liked = true;
            if (lastTrip.usersComment.indexOf(userid) !== -1)
                lastTrip.commented = true;
        }
        
        let result = {
            "message": "Sucess",
            "status" : true,
            "trip"   : lastTrip
        }
        return res.status(200).json(result);
    }).catch(error => {
        let result = {
            "message": "An error occurred",
            "status" : false
        }
        console.log(error);
        return res.status(200).json(result);
    });
})

//API update member wallet in post
app.post('/followTrip', (req, res) => {
    let userid = req.body.userid;
    let tripid = req.body.tripid;

    // Create a reference to the SF doc.
    var sfDocRef = admin.firestore().collection("trips").doc(tripid);
    let trip;
    admin.firestore().runTransaction(transaction => {
        return transaction.get(sfDocRef).then(snapshot => {
            trip = snapshot.data();
            let follow = trip.follow;
            if (follow.indexOf(userid) !==  -1) {
                follow.splice(follow.indexOf(userid), 1);
            } else {
                follow.push(userid);
            }
            trip.follow = follow;
            transaction.update(sfDocRef, 'follow', follow);
            return "TODO";
        });
    }).then(() => {
        console.log("Transaction successfully committed!");
        var result = {};
        result.message = "success";
        result.status = true;
        result.trip = trip;
        return res.status(200).json(result);
    }).catch((error) => {
        console.log("Transaction failed: ", error);
        res.status(200).send("Failed!");
    });
})

// Expose the API as a function
exports.api = functions.https.onRequest(app);